module.exports = {
  title: 'Patient Monitoring',
  tagline: 'Open Source AI project, analysing real time CCTV footage to assist Health care workers and doctors treating ICU patients by alerting them and bringing to notice any emergency activity(gasping, calling for help, falling from bed etc) which may otherwise get delayed attention due to huge influx of patients in ICU and few number of care givers.',
  url: 'https://iotiotdotin.gitlab.io',
  baseUrl: '/patientmonitoring/',
  favicon: 'img/favicon.ico',
 organizationName: 'iotiotdotin', // Usually your GitHub org/user name.
  projectName: 'patientmonitoring', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Patient Monitoring',
      logo: {
        alt: 'Patient Monitoring Logo',
        src: 'img/logo.png',
      },
      links: [
        {to: 'blog', label: 'Blog', position: 'left'},
	{to: 'docs/doc1', label: 'Wiki', position: 'left'},
        {to: 'contributors', label: 'Contributors', position: 'left'},
        {
          href: 'http://bit.ly/aiforicu',
          label: 'Chat',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Community',
          items: [
            {
              label: 'WhatsApp',
              href: 'http://bit.ly/aiforicu',
            },
          ],
        },
        {
          title: 'Social',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/iotiotdotin/patient-monitoring-grp/patient-monitoring-main/-/wikis/home',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} IoTIoT.in.`,
    },
  },
  plugins: [
      [
        '@docusaurus/plugin-ideal-image',
        {
          quality: 70,
          max: 1030, // max resized image's size.
          min: 640, // min resized image's size. if original is lower, use that size.
          steps: 2, // the max number of images generated between min and max (inclusive)
        },
      ],
    ],
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/iotiotdotin/patientmonitoring/-/tree/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
