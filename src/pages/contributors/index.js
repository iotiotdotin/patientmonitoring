import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

import Image from '@theme/IdealImage';


const withImg = [
  {
    thumbnail: require('./data/Sneha.jpg'),
    name: 'Sneha Bhapkar',
    num: '0',
    title: 'AI on Edge Lead',
    text: (
      <>

      </>
    ),
   linkedin: 'https://www.linkedin.com/in/sneha-bhapkar-91183091',
  },
  {
    thumbnail: require('./data/nik.jpg'),
    name: 'Nikhil Bhaskaran',
    num: '0',
    title: 'Lead Project Servant',
    text: (
      <>

      </>
    ),
   linkedin: 'https://www.linkedin.com/in/nikhilbhaskaran/',
  },


  {
    thumbnail: require('./data/blank.png'),
    name: 'Jebastin Nadar',
    num: '0',
    title: 'Human Activity Detection Lead',
    text: (
      <>

      </>
    ),
  },

];

const withImg2 = [
  {
    thumbnail: require('./data/blank.png'),
    name: 'Siddharth',
    num: '1',
    title: 'Motion Detection',
    text: (
      <>

      </>
    ),
  },
 
];

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Contributors<head />">
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">Our Contributors</h1>
          <p className="hero__subtitle">Thanks to everyone for their contribution.</p>
          <p className="hero__subtitle">This Project is thriving due to contributions made by these people.</p>
        </div>
      </header>
      <main>
        <div
          className={classnames(
            styles.section,
            styles.sectionAlt,
          )}>
          <div className="container">
            <div className="row">
              {withImg.map(quote => (
                <div className="col" key={quote.name} >
                  <div className="avatar avatar--vertical margin-bottom--sm">
                    <Image
                      alt={quote.name}
                      className="avatar__photo avatar__photo--xl"
                      img={quote.thumbnail}
                      style={{overflow: 'hidden'}}
                    />
                    <div className="avatar__intro padding-top--sm">
                      <h4 className="avatar__name">{quote.name}</h4>
                      <small className="avatar__subtitle">{quote.title}</small>
                    </div>
                  </div>
		
		  <a href={quote.linkedin}>
            		<p className="text--center text--italic padding-horiz--md">
                        	LinkedIn Profile
            	  	</p>
         	 </a>
                  <p className="text--center text--italic padding-horiz--md">
                    {quote.text}
                  </p>
                </div>
              ))}
            </div>
            <div className="row">
              {withImg2.map(quote => (
                <div className="col" key={quote.name} >
                  <div className="avatar avatar--vertical margin-bottom--sm">
                    <Image
                      alt={quote.name}
                      className="avatar__photo avatar__photo--xl"
                      img={quote.thumbnail}
                      style={{overflow: 'hidden'}}
                    />
                    <div className="avatar__intro padding-top--sm">
                      <h4 className="avatar__name">{quote.name}</h4>
                      <small className="avatar__subtitle">{quote.title}</small>
                    </div>
                  </div>
                  <p className="text--center text--italic padding-horiz--md">
                    {quote.text}
                  </p>
                </div>
              ))}
            </div>
           </div>
        </div>
        <div className={classnames('hero hero--primary-dark', styles.heroBanner)}>
          <div className="container">
              <Link
              className={classnames(
              		'button button--outline button--secondary button--lg',
              		styles.indexCtas,
                  )}
              to={useBaseUrl('docs/contributors')}>
              More...
              </Link>                
          </div>
        </div>
      </main>
    </Layout>
  );
}

export default Home;
